/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:55:38 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:33:43 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FRACTOL_H
# define __FRACTOL_H
# include <mlx.h>
# include <math.h>
# include <stdlib.h>
# include <pthread.h>
# include <sys/types.h>
# include <unistd.h>
# include "keys.h"

# define WID 800.
# define HEI 800.
# define ABS(x) (x < 0 ? -x : x)

typedef struct	s_cycle
{
	double		z_real;
	double		z_img;
	int			iter;
	int			xi;
	int			yi;
	double		x;
	double		y;
	char		*data;
	int			*endian;
	int			*bpp;
	int			*sline;
}				t_cycle;

typedef struct	s_p
{
	void		*mlx;
	void		*win;
	void		*img;
	int			set;
	int			end[4];
	int			prev_xy[2];
	double		center_x;
	double		center_y;
	double		dist;
	int			iterations;
	int			mouse_locked;
	double		precalculated[4];
	int			complex_locked;
	int			zoom_locked[2];
	double		real_c;
	double		img_c;
	int			palette_num;
	int			(*ret_color[8])(int);
	void		(*iter_funcs[3])(t_cycle *, struct s_p *);
	pthread_t	threads[4];
	t_cycle		*cycle[4];
}				t_p;

typedef struct	s_thread_info
{
	int			thread_num;
	t_p			*params;
}				t_thread_info;

void			choose_set(t_p *params, int set);
void			main_cycle(t_p *params);

void			mdbrt_iter(t_cycle *t, t_p *p);
void			julia_iter(t_cycle *t, t_p *p);
void			burning_ship_iter(t_cycle *t, t_p *p);

void			lock_complex(t_p *params);
void			trigger_zoom(t_p *p, int trigger);
void			change_palette(t_p *p);
void			assign_palettes(t_p *params);
void			zoom_to_mouse(t_p *p, int x, int y, int button);
void			reset(t_p *p);

/*
**	Keyhooks
*/

int				buttons(int button, void *param);
void			keyhook_help(int button, t_p *params);
int				press(int button, int x, int y, void *param);
int				release(int button, int x, int y, void *param);
int				release_key(int button, void *param);
int				loop_hook(void *p);
int				exit_hook(void *p);

/*
**	Managing color palettes
*/

int				ret_color_std(int iter);
int				ret_color_red(int iter);
int				ret_color_pls_end(int iter);
int				ret_color_drugs(int iter);

/*
**	Threads
*/

void			generate_threads(t_p *params);
void			wait_for_threads(t_p *params);
void			*thread_cycle(void *p);

/*
**	ARGV parsing
*/

int				check_args(int argc, char **argv);
void			fractol_usage(void);

/*
**	A little bit of supporting functions
*/

int				ft_atoi(char const *s);
void			p_to_img(unsigned int img_c, t_cycle *cycle);
#endif
