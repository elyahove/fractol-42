/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal_iterations.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 18:10:22 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:18:16 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		julia_iter(t_cycle *t, t_p *p)
{
	t->iter = 0;
	while (t->iter < p->iterations)
	{
		t->z_real = t->x * t->x - t->y * t->y + p->real_c;
		t->z_img = 2 * t->x * t->y + p->img_c;
		t->x = t->z_real;
		t->y = t->z_img;
		if ((t->x * t->x) + (t->y * t->y) > 4)
			break ;
		t->iter++;
	}
	if (t->iter < 2 || t->iter == p->iterations)
		p_to_img(0x0, t);
	else
		p_to_img((unsigned int)p->ret_color[p->palette_num](t->iter), t);
}

void		mdbrt_iter(t_cycle *t, t_p *p)
{
	double		tx;
	double		ty;

	t->iter = 0;
	tx = t->x;
	ty = t->y;
	while (t->iter < p->iterations)
	{
		t->z_real = t->x * t->x - t->y * t->y;
		t->z_img = 2 * t->x * t->y;
		t->x = t->z_real + tx;
		t->y = t->z_img + ty;
		if ((t->x * t->x) + (t->y * t->y) > 4)
			break ;
		t->iter++;
	}
	if (t->iter != p->iterations)
		p_to_img((unsigned int)p->ret_color[p->palette_num](t->iter), t);
	else
		p_to_img(0x0, t);
}

void		burning_ship_iter(t_cycle *t, t_p *p)
{
	double		tx;
	double		ty;

	t->iter = 0;
	tx = t->x;
	ty = t->y;
	while (t->iter < p->iterations)
	{
		t->z_real = t->x * t->x - t->y * t->y;
		t->z_img = 2 * ABS(t->x * t->y);
		t->x = t->z_real + tx;
		t->y = t->z_img + ty;
		if ((t->x * t->x) + (t->y * t->y) > 4)
			break ;
		t->iter++;
	}
	if (t->iter != p->iterations)
		p_to_img((unsigned int)p->ret_color[p->palette_num](t->iter), t);
	else
		p_to_img(0x0, t);
}

