/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_argv.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 18:24:32 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:25:19 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		check_args(int argc, char **argv)
{
	int		i;
	int		j;

	i = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
		{
			if (argv[i][j] < '0' || argv[i][j] > '9')
				return (0);
			if (j == 0 && argv[i][j] == '0')
				return (0);
			j++;
		}
		if (ft_atoi(argv[i]) < 1 || ft_atoi(argv[i]) > 3)
			return (0);
		i++;
	}
	return (1);
}

void	fractol_usage(void)
{
	write(1, "Usage: ./fractol {num}\n", 23);
	write(1, "\t1 -> Mandelbrot\n", 17);
	write(1, "\t2 -> Julia\n", 12);
	write(1, "\t3 -> Burning Ship\n", 19);
	exit (0);
}
