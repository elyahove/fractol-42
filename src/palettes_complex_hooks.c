/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 15:46:57 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:32:09 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		complex_assist(int button, t_p *params)
{
	if (button == KEY_3)
	{
		params->real_c = .3;
		params->img_c = -.01;
	}
	else if (button == KEY_4)
	{
		params->real_c = -1.476;
		params->img_c = 0;
	}
	else if (button == KEY_5)
	{
		params->real_c = -.12;
		params->img_c = -.77;
	}
	else if (button == KEY_6)
	{
		params->real_c = .28;
		params->img_c = .008;
	}
}

void		keyhook_help(int button, t_p *params)
{
	if (button == KEY_P)
		change_palette(params);
	else if (button == KEY_UP)
		params->center_y += params->dist * 0.05;
	else if (button == KEY_DOWN)
		params->center_y -= params->dist * 0.05;
	else if (button == KEY_RIGHT)
		params->center_x -= params->dist * 0.05;
	else if (button == KEY_LEFT)
		params->center_x += params->dist * 0.05;
	else if (button == KEY_1)
	{
		params->real_c = -.79;
		params->img_c = .15;
	}
	else if (button == KEY_2)
	{
		params->real_c = -.162;
		params->img_c = 1.04;
	}
	else
		complex_assist(button, params);
}

void		assign_palettes(t_p *p)
{
	p->ret_color[0] = &ret_color_std;
	p->ret_color[1] = &ret_color_red;
	p->ret_color[2] = &ret_color_drugs;
	p->ret_color[3] = &ret_color_pls_end;
	p->palette_num = 0;
}

void		lock_complex(t_p *params)
{
	params->complex_locked = 1;
}
