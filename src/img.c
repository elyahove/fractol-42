/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   c_p_to_img.c.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/22 20:12:45 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:14:53 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <stdio.h>

void	p_to_img(unsigned int img_c, t_cycle *t)
{
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;
	int				len;

	r = ((img_c & 0xFF0000) >> 16);
	g = ((img_c & 0xFF00) >> 8);
	b = (img_c & 0xFF);
	len = (t->yi * 800 + t->xi) * 4;
	t->data[len] = b;
	t->data[len + 1] = g;
	t->data[len + 2] = r;
}
