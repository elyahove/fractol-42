/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 17:07:12 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:55:28 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			precalculate_cycle_constants(t_p *params)
{
	params->precalculated[0] = params->center_x - params->dist;
	params->precalculated[1] = params->dist + params->dist;
	params->precalculated[2] = params->center_y - params->dist;
	params->precalculated[3] = params->dist + params->dist;
}

void			main_cycle(t_p *params)
{
	precalculate_cycle_constants(params);
	generate_threads(params);
	wait_for_threads(params);
	mlx_put_image_to_window(params->mlx, params->win, params->img, 0, 0);
}

int				buttons(int button, void *param)
{
	t_p		*p;

	p = (t_p*)param;
	if (button == KEY_PAD_ADD)
		p->iterations += 10;
	else if (button == KEY_PAD_SUB)
		p->iterations -= 10;
	else if (button == KEY_PAGE_UP)
		p->dist *= 0.99;
	else if (button == KEY_PAGE_DOWN)
		p->dist *= 1.01;
	else if (button == KEY_Z)
		lock_complex(p);
	else if (button == KEY_I)
		trigger_zoom(p, 0);
	else if (button == KEY_O)
		trigger_zoom(p, 1);
	else if (button == KEY_ESCAPE)
		exit_hook(p);
	else if (button == KEY_R)
		reset(p);
	else
		keyhook_help(button, p);
	main_cycle(p);
	return (0);
}
