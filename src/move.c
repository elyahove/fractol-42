/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 15:52:18 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:33:08 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			press(int button, int x, int y, void *param)
{
	t_p		*p;

	p = (t_p*)param;
	if (button == 1)
	{
		p->mouse_locked = 1;
		p->prev_xy[0] = x;
		p->prev_xy[1] = y;
		return (0);
	}
	else if (button == 5)
		zoom_to_mouse(p, x, y, 5);
	else if (button == 4)
		zoom_to_mouse(p, x, y, 4);
	main_cycle(p);
	return (0);
}

int			release(int button, int x, int y, void *param)
{
	t_p		*p;

	p = (t_p*)param;
	(void)x;
	(void)y;
	if (button == 1)
		p->mouse_locked = 0;
	return (0);
}

int			release_key(int button, void *param)
{
	t_p		*p;

	p = (t_p*)param;
	if (button == KEY_Z)
		p->complex_locked = 0;
	return (0);
}
