/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   palettes.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 19:23:19 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:36:33 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			ret_color_std(int iter)
{
	int		palette[16];

	palette[0] = 0x0;
	palette[1] = 0x19071A;
	palette[2] = 0x09012F;
	palette[3] = 0x040449;
	palette[4] = 0x000764;
	palette[5] = 0x0C2C8A;
	palette[6] = 0x1852B1;
	palette[7] = 0x397DD1;
	palette[8] = 0x86B5E5;
	palette[9] = 0xD3ECF8;
	palette[10] = 0xF1E9BF;
	palette[11] = 0xF8C95F;
	palette[12] = 0xFFAA00;
	palette[13] = 0xCC8000;
	palette[14] = 0x995700;
	palette[15] = 0x6A3403;
	return (palette[iter % 16]);
}

int			ret_color_red(int iter)
{
	int		palette[16];

	palette[0] = 0xFF0000;
	palette[1] = 0xFF1000;
	palette[2] = 0xFF2000;
	palette[3] = 0xFF3000;
	palette[4] = 0xFF4000;
	palette[5] = 0xFF5000;
	palette[6] = 0xFF6000;
	palette[7] = 0xFF7000;
	palette[8] = 0xFF8000;
	palette[9] = 0xFF9000;
	palette[10] = 0xFFA000;
	palette[11] = 0xFFB000;
	palette[12] = 0xFFC000;
	palette[13] = 0xFFD000;
	palette[14] = 0xFFE000;
	palette[15] = 0xFFF000;
	return (palette[iter % 16]);
}

int			ret_color_drugs(int iter)
{
	int		palette[16];

	palette[0] = 0x00FF00;
	palette[1] = 0x10DA50;
	palette[2] = 0x200210;
	palette[3] = 0x301488;
	palette[4] = 0x405922;
	palette[5] = 0x50ADAD;
	palette[6] = 0x60AD90;
	palette[7] = 0x70BBCC;
	palette[8] = 0x80DABC;
	palette[9] = 0x902121;
	palette[10] = 0xA032A3;
	palette[11] = 0xB0AB12;
	palette[12] = 0xC08243;
	palette[13] = 0xD06555;
	palette[14] = 0xE01341;
	palette[15] = 0xF0FFFF;
	return (palette[iter % 16]);
}

int			ret_color_pls_end(int iter)
{
	int		palette[64];

	palette[0] = 0x000000;
	palette[1] = 0x000404;
	palette[2] = 0x000808;
	palette[3] = 0x000C0C;
	palette[4] = 0x001010;
	palette[5] = 0x001414;
	palette[6] = 0x001818;
	palette[7] = 0x001C1C;
	palette[8] = 0x002020;
	palette[9] = 0x002424;
	palette[10] = 0x002828;
	palette[11] = 0x002C2C;
	palette[12] = 0x003030;
	palette[13] = 0x003434;
	palette[14] = 0x003838;
	palette[15] = 0x003C3C;
	palette[16] = 0x004040;
	palette[17] = 0x004444;
	palette[18] = 0x004848;
	palette[19] = 0x004C4C;
	palette[20] = 0x005050;
	palette[21] = 0x005454;
	palette[22] = 0x005858;
	palette[23] = 0x005C5C;
	palette[24] = 0x006060;
	palette[25] = 0x006464;
	palette[26] = 0x006868;
	palette[27] = 0x006C6C;
	palette[28] = 0x007070;
	palette[29] = 0x007474;
	palette[30] = 0x007878;
	palette[31] = 0x007C7C;
	palette[32] = 0x008080;
	palette[33] = 0x008484;
	palette[34] = 0x008888;
	palette[35] = 0x008C8C;
	palette[36] = 0x009090;
	palette[37] = 0x009494;
	palette[38] = 0x009898;
	palette[39] = 0x009C9C;
	palette[40] = 0x00A0A0;
	palette[41] = 0x00A4A4;
	palette[42] = 0x00A8A8;
	palette[43] = 0x00ACAC;
	palette[44] = 0x00B0B0;
	palette[45] = 0x00B4B4;
	palette[46] = 0x00B8B8;
	palette[47] = 0x00BCBC;
	palette[48] = 0x00C0C0;
	palette[49] = 0x00C4C4;
	palette[50] = 0x00C8C8;
	palette[51] = 0x00CCCC;
	palette[52] = 0x00D0D0;
	palette[53] = 0x00D4D4;
	palette[54] = 0x00D8D8;
	palette[55] = 0x00DCDC;
	palette[56] = 0x00E0E0;
	palette[57] = 0x00E4E4;
	palette[58] = 0x00E8E8;
	palette[59] = 0x00ECEC;
	palette[60] = 0x00F0F0;
	palette[61] = 0x00F4F4;
	palette[62] = 0x00F8F8;
	palette[63] = 0x00FCFC;
	return (palette[iter % 64]);
}

void		change_palette(t_p *p)
{
	p->palette_num++;
	if (p->palette_num >= 4)
		p->palette_num = 0;
}
