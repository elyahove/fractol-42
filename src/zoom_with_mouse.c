/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   zoom_with_mouse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 18:53:18 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:36:05 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	zoom_to_mouse(t_p *p, int x, int y, int button)
{
	double		xy[2];
	double		x_min_max[2];
	double		y_min_max[2];
	double		mult;

	mult = (button == 4 ? 1.1 : 0.9);
	xy[0] = x / WID * (p->dist * 2) + p->center_x - p->dist;
	xy[1] = y / HEI * (p->dist * 2) + p->center_y - p->dist;
	x_min_max[0] = (p->center_x - p->dist) * mult + xy[0] * (1 - mult);
	x_min_max[1] = (p->center_x + p->dist) * mult + xy[0] * (1 - mult);
	y_min_max[0] = (p->center_y - p->dist) * mult + xy[1] * (1 - mult);
	y_min_max[1] = (p->center_y + p->dist) * mult + xy[1] * (1 - mult);
	p->dist = (x_min_max[1] - x_min_max[0]) / 2;
	p->center_x = (x_min_max[1] - p->dist);
	p->center_y = (y_min_max[1] - p->dist);
}
