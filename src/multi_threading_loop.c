/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   multi_threading_loop.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 17:07:21 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:55:16 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <stdio.h>

void	*thread_cycle(void *p)
{
	t_thread_info	*thread;
	t_p				*params;
	t_cycle			*t;

	thread = (t_thread_info*)p;
	params = thread->params;
	t = params->cycle[thread->thread_num];
	t->yi = (HEI / 4) * thread->thread_num;
	while (t->yi < ((HEI / 4) * (thread->thread_num + 1)))
	{
		t->xi = 0;
		while (t->xi < WID - 1)
		{
			t->x = (double)t->xi / WID
				* params->precalculated[1]
				+ params->precalculated[0];
			t->y = (double)t->yi / HEI
				* params->precalculated[3]
				+ params->precalculated[2];
			params->iter_funcs[params->set - 1](t, params);
			t->xi++;
		}
		t->yi++;
	}
	free(p);
	pthread_exit(NULL);
}
