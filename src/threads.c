/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   threads.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 18:21:33 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:55:46 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

t_thread_info	*generate_thread_info(t_p *params, int thread_num)
{
	t_thread_info	*res;

	res = (t_thread_info*)malloc(sizeof(t_thread_info));
	res->params = params;
	res->thread_num = thread_num;
	return (res);
}

void			generate_threads(t_p *params)
{
	int		i;

	i = -1;
	while (++i < 4)
		pthread_create(&params->threads[i], NULL, thread_cycle,
				generate_thread_info(params, i));
}

void			wait_for_threads(t_p *params)
{
	int		i;

	i = -1;
	while (++i < 4)
		pthread_join(params->threads[i], NULL);
}

