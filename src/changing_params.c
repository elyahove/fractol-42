/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   changing_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/15 15:52:44 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:59:02 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <unistd.h>

void	trigger_zoom(t_p *params, int trigger)
{
	if (trigger == 0)
	{
		params->zoom_locked[0] = params->zoom_locked[0] ? 0 : 1;
		params->zoom_locked[1] = 0;
	}
	else if (trigger == 1)
	{
		params->zoom_locked[1] = params->zoom_locked[1] ? 0 : 1;
		params->zoom_locked[0] = 0;
	}
}

int		loop_hook(void *p)
{
	t_p		*params;

	params = (t_p*)p;
	if (params->zoom_locked[0])
		params->dist *= 0.95;
	else if (params->zoom_locked[1])
		params->dist *= 1.05;
	main_cycle(params);
	return (0);
}

int		exit_hook(void *p)
{
	t_p		*temp;
	int		i;

	temp = (t_p*)p;
	i = 0;
	while (i < 4)
	{
		free(temp->cycle[i]->bpp);
		free(temp->cycle[i]->sline);
		free(temp->cycle[i]->endian);
		free(temp->cycle[i]);
		i++;
	}
	free(p);
	exit(0);
}

void	reset(t_p *params)
{
	params->dist = 2.;
	params->center_x = 0;
	params->center_y = 0;
	params->prev_xy[0] = WID / 2;
	params->prev_xy[1] = HEI / 2;
	params->iterations = 100;
	params->mouse_locked = 0;
	params->zoom_locked[0] = 0;
	params->zoom_locked[1] = 0;
	params->complex_locked = 0;
}
