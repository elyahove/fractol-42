/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/13 16:57:59 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:42:06 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mach/task_info.h>
#include "fractol.h"

t_p		*initialize_params(void)
{
	t_p		*params;

	params = (t_p*)malloc(sizeof(t_p));
	params->mlx = mlx_init();
	params->win = NULL;
	params->iterations = 100;
	params->prev_xy[0] = 400;
	params->prev_xy[1] = 400;
	params->center_x = 0.;
	params->center_y = 0.;
	params->dist = 2.;
	params->mouse_locked = 0;
	params->complex_locked = 0;
	params->zoom_locked[0] = 0;
	params->zoom_locked[1] = 0;
	params->real_c = -.79;
	params->img_c = .15;
	return (params);
}

int		main(int argc, char **argv)
{
	t_p		*params;
	pid_t	pid;

	pid = 1;
	if (argc < 2 || argc > 3 || !check_args(argc, argv))
		fractol_usage();
	if (argc == 3)
		pid = fork();
	params = initialize_params();
	choose_set(params, argv[pid == 0 ? 2 : 1][0] - '0');
	/*if (argv[pid == 0 ? 2 : 1][0] == '1')
		choose_set(params, 1);
	else if (argv[pid == 0 ? 2 : 1][0] == '2')
		choose_set(params, 2);
	else if (argv[pid == 0 ? 2 : 1][0] == '3')
		choose_set(params, 3);
		*/
}
