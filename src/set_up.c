/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_up.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elyahove <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/17 18:58:49 by elyahove          #+#    #+#             */
/*   Updated: 2017/06/16 18:56:08 by elyahove         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			mouse_movement_events(int x, int y, void *param)
{
	t_p		*params;

	params = (t_p*)param;
	if (x > WID || x < 0 || y > HEI || y < 0)
		return (0);
	params->precalculated[0] = params->dist * 2;
	if (params->set == 2 && params->complex_locked && params->mouse_locked == 0)
	{
		params->real_c = (double)x / WID * params->precalculated[0]
			+ params->center_x - params->dist;
		params->img_c = (double)y / HEI * params->precalculated[0]
			+ params->center_y - params->dist;
		main_cycle(params);
	}
	if (params->mouse_locked != 0)
	{
		params->center_x += ((params->prev_xy[0] - (double)x)
						* params->dist / WID) * 2;
		params->center_y += ((params->prev_xy[1] - (double)y)
						* params->dist / HEI) * 2;
		params->prev_xy[0] = x;
		params->prev_xy[1] = y;
		main_cycle(params);
	}
	return (1);
}

void		assign_iteration_functions(t_p *p)
{
	p->iter_funcs[0] = &mdbrt_iter;
	p->iter_funcs[1] = &julia_iter;
	p->iter_funcs[2] = &burning_ship_iter;
}

t_cycle		*assign_t_cycle(t_p *p)
{
	t_cycle	*res;

	res = (t_cycle*)malloc(sizeof(t_cycle));
	res->endian = (int*)malloc(sizeof(int));
	res->bpp = (int*)malloc(sizeof(int));
	res->sline = (int*)malloc(sizeof(int));
	res->data = mlx_get_data_addr(p->img, res->bpp, res->sline,
			res->endian);
	return (res);
}

char		*return_name(int set)
{
	if (set == 1)
		return ("Mandelbrot");
	else if (set == 2)
		return ("Julia");
	else if (set == 3)
		return ("Burning ship");
	return ("lol");
}

void		choose_set(t_p *p, int set)
{
	int		i;

	p->set = set;
	if (p->win == NULL)
		p->win = mlx_new_window(p->mlx, WID, HEI, return_name(p->set));
	p->img = mlx_new_image(p->mlx, 800, 800);
	assign_iteration_functions(p);
	assign_palettes(p);
	i = 0;
	while (i < 4)
		p->cycle[i++] = assign_t_cycle(p);
	main_cycle(p);
	mlx_hook(p->win, 6, 0, &mouse_movement_events, p);
	mlx_hook(p->win, 4, 0, &press, p);
	mlx_hook(p->win, 5, 0, &release, p);
	mlx_hook(p->win, 3, 0, &release_key, p);
	mlx_hook(p->win, 2, 0, &buttons, p);
	mlx_hook(p->win, 17, 0, &exit_hook, p);
	mlx_loop_hook(p->mlx, &loop_hook, p);
	mlx_loop(p->mlx);
}
