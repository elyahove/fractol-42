# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: elyahove <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/13 17:00:43 by elyahove          #+#    #+#              #
#    Updated: 2017/06/16 19:55:37 by elyahove         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME 		= fractol
FLAGS		= -Wall -Werror -Wextra
FRAMEWORKS 	= -framework AppKit -framework OpenGL

#===================================================================#

SRC_FILES 	= main.c img.c main_loop.c palettes_complex_hooks.c changing_params.c \
	 	  	  move.c set_up.c zoom_with_mouse.c secondary.c threads.c palettes.c  \
	  	  	  multi_threading_loop.c parsing_argv.c fractal_iterations.c

LIBMLX		= libmlx.a
SRC 		= $(addprefix $(SRC_PATH),$(SRC_FILES))
OBJ			= $(SRC:.c=.o)

#================================PROJECT PATHS=================================#

LIB_PATH 	= ./libs/
SRC_PATH	= ./src/
INCL_PATH	= ./includes/
MLX_PATH 	= $(LIB_PATH)minilibx_macos

#==============================================================================#

.PHONY: mlx

$(NAME): mlx $(OBJ)
	gcc $(FLAGS) $(OBJ) -o $(NAME) $(LIBMLX) $(FRAMEWORKS) -I$(INCL_PATH)

mlx:
	make -C $(MLX_PATH)
	mv $(MLX_PATH)/$(LIBMLX) ./


%.o:%.c
	gcc $(FLAGS) -c $< -o $@ -I$(INCL_PATH)

all: $(NAME)

clean:
	make clean -C $(MLX_PATH)
	rm -f $(OBJ)

fclean: clean
	rm $(LIBMLX)
	rm -f $(NAME)

re: fclean all
